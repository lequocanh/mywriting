package Utils.SQL;

public class SQLDB {

	/**
	 * @uml.property  name="hostIP"
	 */
	private String hostIP;
	/**
	 * @uml.property  name="hostPORT"
	 */
	private String hostPORT;
	/**
	 * @uml.property  name="dbName"
	 */
	private String dbName;
	
	public SQLDB(String hostIP, String hostPORT, String dbName)
	{
		this.hostIP = hostIP;
		this.hostPORT = hostPORT;
		this.dbName = dbName;
	}
	
	/**
	 * @return
	 * @uml.property  name="hostIP"
	 */
	public String getHostIP ()
	{
		return this.hostIP;
	}
	/**
	 * @return
	 * @uml.property  name="hostPORT"
	 */
	public String getHostPORT ()
	{
		return this.hostPORT;
	}
	public String getDBName ()
	{
		return this.dbName;
	}
	
}
