package Utils.SQL;

public class couple 
	{
		/**
		 * @uml.property  name="e1"
		 */
		private Object e1;
		/**
		 * @uml.property  name="e2"
		 */
		private Object e2;
		
		public couple(Object e1, Object e2)
		{
			this.e1 = e1;
			this.e2 = e2;
		}
		
		public Object getFirst() {return this.e1;}
		public Object getLast() {return this.e2;}
	}