package Utils;

public class Quantite {

	private String quantite;

	public Quantite()
	{
		this.quantite = null;
	}
	
	public Quantite(int quantite)
	{
		this.quantite = new String(new Integer(quantite).toString());
	}
	
	public String getQuantite()
	{
		return this.quantite;
	}
	
}
