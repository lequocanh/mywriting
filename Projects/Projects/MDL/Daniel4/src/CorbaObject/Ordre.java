// **********************************************************************
//
// Generated by the ORBacus IDL to Java Translator
//
// Copyright (c) 2005
// IONA Technologies, Inc.
// Waltham, MA, USA
//
// All Rights Reserved
//
// **********************************************************************

// Version: 4.3.2

package CorbaObject;

//
// IDL:CorbaObject/Ordre:1.0
//
/***/

final public class Ordre implements org.omg.CORBA.portable.IDLEntity
{
    private static final String _ob_id = "IDL:CorbaObject/Ordre:1.0";

    public
    Ordre()
    {
    }

    public
    Ordre(String numeroParticulier,
          boolean estUnAchat,
          boolean estAToutPrix,
          String numeroSociete,
          int quantiteDesiree,
          int quantiteRealisee,
          float montant,
          DateStruct dEmission,
          DateStruct dButoir,
          DateStruct dRealisation,
          String etat)
    {
        this.numeroParticulier = numeroParticulier;
        this.estUnAchat = estUnAchat;
        this.estAToutPrix = estAToutPrix;
        this.numeroSociete = numeroSociete;
        this.quantiteDesiree = quantiteDesiree;
        this.quantiteRealisee = quantiteRealisee;
        this.montant = montant;
        this.dEmission = dEmission;
        this.dButoir = dButoir;
        this.dRealisation = dRealisation;
        this.etat = etat;
    }

    public String numeroParticulier;
    public boolean estUnAchat;
    public boolean estAToutPrix;
    public String numeroSociete;
    public int quantiteDesiree;
    public int quantiteRealisee;
    public float montant;
    public DateStruct dEmission;
    public DateStruct dButoir;
    public DateStruct dRealisation;
    public String etat;
}
