// **********************************************************************
//
// Generated by the ORBacus IDL to Java Translator
//
// Copyright (c) 2005
// IONA Technologies, Inc.
// Waltham, MA, USA
//
// All Rights Reserved
//
// **********************************************************************

// Version: 4.3.2

package CorbaObject;

//
// IDL:CorbaObject/CourtierForParticulier:1.0
//
public abstract class CourtierForParticulierPOA
    extends org.omg.PortableServer.Servant
    implements org.omg.CORBA.portable.InvokeHandler,
               CourtierForParticulierOperations
{
    static final String[] _ob_ids_ =
    {
        "IDL:CorbaObject/CourtierForParticulier:1.0",
    };

    public CourtierForParticulier
    _this()
    {
        return CourtierForParticulierHelper.narrow(super._this_object());
    }

    public CourtierForParticulier
    _this(org.omg.CORBA.ORB orb)
    {
        return CourtierForParticulierHelper.narrow(super._this_object(orb));
    }

    public String[]
    _all_interfaces(org.omg.PortableServer.POA poa, byte[] objectId)
    {
        return _ob_ids_;
    }

    public org.omg.CORBA.portable.OutputStream
    _invoke(String opName,
            org.omg.CORBA.portable.InputStream in,
            org.omg.CORBA.portable.ResponseHandler handler)
    {
        final String[] _ob_names =
        {
            "ConsulterBourse",
            "EmettreOrdre",
            "TransfererLiquidite",
            "TransfererTitre",
            "checkLogin",
            "getListCompteLiquidite",
            "getListCompteTitre",
            "getListCourtier",
            "getListOrdre",
            "getListSociete",
            "getListSocietePourUnParticulier",
            "getListVirement"
        };

        int _ob_left = 0;
        int _ob_right = _ob_names.length;
        int _ob_index = -1;

        while(_ob_left < _ob_right)
        {
            int _ob_m = (_ob_left + _ob_right) / 2;
            int _ob_res = _ob_names[_ob_m].compareTo(opName);
            if(_ob_res == 0)
            {
                _ob_index = _ob_m;
                break;
            }
            else if(_ob_res > 0)
                _ob_right = _ob_m;
            else
                _ob_left = _ob_m + 1;
        }

        if(_ob_index == -1 && opName.charAt(0) == '_')
        {
            _ob_left = 0;
            _ob_right = _ob_names.length;
            String _ob_ami_op =
                opName.substring(1);

            while(_ob_left < _ob_right)
            {
                int _ob_m = (_ob_left + _ob_right) / 2;
                int _ob_res = _ob_names[_ob_m].compareTo(_ob_ami_op);
                if(_ob_res == 0)
                {
                    _ob_index = _ob_m;
                    break;
                }
                else if(_ob_res > 0)
                    _ob_right = _ob_m;
                else
                    _ob_left = _ob_m + 1;
            }
        }

        switch(_ob_index)
        {
        case 0: // ConsulterBourse
            return _OB_op_ConsulterBourse(in, handler);

        case 1: // EmettreOrdre
            return _OB_op_EmettreOrdre(in, handler);

        case 2: // TransfererLiquidite
            return _OB_op_TransfererLiquidite(in, handler);

        case 3: // TransfererTitre
            return _OB_op_TransfererTitre(in, handler);

        case 4: // checkLogin
            return _OB_op_checkLogin(in, handler);

        case 5: // getListCompteLiquidite
            return _OB_op_getListCompteLiquidite(in, handler);

        case 6: // getListCompteTitre
            return _OB_op_getListCompteTitre(in, handler);

        case 7: // getListCourtier
            return _OB_op_getListCourtier(in, handler);

        case 8: // getListOrdre
            return _OB_op_getListOrdre(in, handler);

        case 9: // getListSociete
            return _OB_op_getListSociete(in, handler);

        case 10: // getListSocietePourUnParticulier
            return _OB_op_getListSocietePourUnParticulier(in, handler);

        case 11: // getListVirement
            return _OB_op_getListVirement(in, handler);
        }

        throw new org.omg.CORBA.BAD_OPERATION();
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_ConsulterBourse(org.omg.CORBA.portable.InputStream in,
                           org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        Evolution _ob_r = ConsulterBourse(_ob_a0);
        out = handler.createReply();
        EvolutionHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_EmettreOrdre(org.omg.CORBA.portable.InputStream in,
                        org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        String _ob_a1 = in.read_string();
        Ordre _ob_a2 = OrdreHelper.read(in);
        Succes _ob_r = EmettreOrdre(_ob_a0, _ob_a1, _ob_a2);
        out = handler.createReply();
        SuccesHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_TransfererLiquidite(org.omg.CORBA.portable.InputStream in,
                               org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        Virement _ob_a1 = VirementHelper.read(in);
        String _ob_a2 = in.read_string();
        Succes _ob_r = TransfererLiquidite(_ob_a0, _ob_a1, _ob_a2);
        out = handler.createReply();
        SuccesHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_TransfererTitre(org.omg.CORBA.portable.InputStream in,
                           org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        String _ob_a1 = in.read_string();
        String _ob_a2 = in.read_string();
        int _ob_a3 = in.read_long();
        String _ob_a4 = in.read_string();
        String _ob_a5 = in.read_string();
        Succes _ob_r = TransfererTitre(_ob_a0, _ob_a1, _ob_a2, _ob_a3, _ob_a4, _ob_a5);
        out = handler.createReply();
        SuccesHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_checkLogin(org.omg.CORBA.portable.InputStream in,
                      org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        String _ob_a1 = in.read_string();
        String _ob_a2 = in.read_string();
        Succes _ob_r = checkLogin(_ob_a0, _ob_a1, _ob_a2);
        out = handler.createReply();
        SuccesHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_getListCompteLiquidite(org.omg.CORBA.portable.InputStream in,
                                  org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        String _ob_a1 = in.read_string();
        CompteDeLiquidite[] _ob_r = getListCompteLiquidite(_ob_a0, _ob_a1);
        out = handler.createReply();
        CompteDeLiquiditeListHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_getListCompteTitre(org.omg.CORBA.portable.InputStream in,
                              org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        String _ob_a1 = in.read_string();
        String _ob_a2 = in.read_string();
        CompteDeTitre[] _ob_r = getListCompteTitre(_ob_a0, _ob_a1, _ob_a2);
        out = handler.createReply();
        CompteDeTitreListHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_getListCourtier(org.omg.CORBA.portable.InputStream in,
                           org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String[] _ob_r = getListCourtier();
        out = handler.createReply();
        StringListHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_getListOrdre(org.omg.CORBA.portable.InputStream in,
                        org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        DateStruct _ob_a1 = DateStructHelper.read(in);
        Ordre[] _ob_r = getListOrdre(_ob_a0, _ob_a1);
        out = handler.createReply();
        OrdreListHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_getListSociete(org.omg.CORBA.portable.InputStream in,
                          org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        Societe[] _ob_r = getListSociete();
        out = handler.createReply();
        SocieteListHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_getListSocietePourUnParticulier(org.omg.CORBA.portable.InputStream in,
                                           org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        String _ob_a1 = in.read_string();
        Societe[] _ob_r = getListSocietePourUnParticulier(_ob_a0, _ob_a1);
        out = handler.createReply();
        SocieteListHelper.write(out, _ob_r);
        return out;
    }

    private org.omg.CORBA.portable.OutputStream
    _OB_op_getListVirement(org.omg.CORBA.portable.InputStream in,
                           org.omg.CORBA.portable.ResponseHandler handler)
    {
        org.omg.CORBA.portable.OutputStream out = null;
        String _ob_a0 = in.read_string();
        DateStruct _ob_a1 = DateStructHelper.read(in);
        Virement[] _ob_r = getListVirement(_ob_a0, _ob_a1);
        out = handler.createReply();
        VirementListHelper.write(out, _ob_r);
        return out;
    }
}
