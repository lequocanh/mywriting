// **********************************************************************
//
// Generated by the ORBacus IDL to Java Translator
//
// Copyright (c) 2005
// IONA Technologies, Inc.
// Waltham, MA, USA
//
// All Rights Reserved
//
// **********************************************************************

// Version: 4.3.2

package CorbaObject;

//
// IDL:CorbaObject/CompteDeLiquidite:1.0
//
final public class CompteDeLiquiditeHolder implements org.omg.CORBA.portable.Streamable
{
    public CompteDeLiquidite value;

    public
    CompteDeLiquiditeHolder()
    {
    }

    public
    CompteDeLiquiditeHolder(CompteDeLiquidite initial)
    {
        value = initial;
    }

    public void
    _read(org.omg.CORBA.portable.InputStream in)
    {
        value = CompteDeLiquiditeHelper.read(in);
    }

    public void
    _write(org.omg.CORBA.portable.OutputStream out)
    {
        CompteDeLiquiditeHelper.write(out, value);
    }

    public org.omg.CORBA.TypeCode
    _type()
    {
        return CompteDeLiquiditeHelper.type();
    }
}
