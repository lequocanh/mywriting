<html><head><link rel="alternate" type="application/rss+xml" title="KhoaHoc.com.vn - Kham pha tri thuc nhan loai" href="http://www.khoahoc.com.vn/rss/">


<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="KhoaHoc.com.vn: Khám phá tri thức nhân loại, bí ẩn cuộc sống, các công cuộc chiinh phục vũ trụ của loài người. Tất cả đều được Khoa Học (KhoaHoc.com.vn) giới thiệu và cung cấp miễn phí.">
<meta name="keywords" content="KhoaHoc.com.vn,Khoa Hoc,Khoa Học,tri thuc,tri thức,mạng tri thức,mang tri thuc,tìm hiểu khoa học,khoa học kỹ thuật,khoa hoc ky thuat,khoa học và cuộc sống,khoa hoc va cuoc song,y học,y học,sức khỏe,suc khoe,phát minh khoa học,phat minh khoa hoc,trí tuệ nhân tạo,">
<meta name="robots" content="INDEX,FOLLOW,ALL"> 
<meta name="language" content="Vietnamese,English">
<meta name="author" content="KhoaHoc.com.vn">
<meta name="copyright" content="Copyright (C) 2005 KhoaHoc.com.vn">
<meta name="revisit-after" content="1 day">
<meta name="document-rating" content="General">
<meta name="document-distribution" content="Global">
<meta name="distribution" content="Global">
<meta name="area" content="Science"> 
<meta name="placename" content="Vietnam"> 
<meta name="resource-type" content="Document"> 
<meta name="owner" content="www.KhoaHoc.com.vn">
<meta name="classification" content="Science,Technology,High Tech,Scientist">
<meta name="rating" content="All">
<meta id="refresher" http-equiv="REFRESH" content="3600">
<link href="kh_files/styles.css" rel="stylesheet" type="text/css">
<link rel="SHORTCUT ICON" href="http://www.khoahoc.com.vn/favicon.ico">
<script src="kh_files/MetaNET_AdObjects.js" type="text/javascript"></script>
<script src="kh_files/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-142769-2";
urchinTracker();
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"><style type="text/css">
.articleTools {
	border-left: 1px solid #EAE8E9;
	border-right: 1px solid #EAE8E9;
	float: right;                                   
	margin: 5px 0px 5px 5px;
	width: 300px;
	}

.toolsContainer {
	margin: 0px 0px -2px 0px;
	border-top: 1px solid #EAE8E9;
	border-bottom: 1px solid #EAE8E9;
	position: relative;
	top: -1px;
	}

.articleTools .toolsContainer ul.toolsList li {
	margin-bottom: 1px;
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 80%; 
	line-height: 1.4em;
	text-transform: uppercase;  
	list-style-image: url('none') !important;padding-left:0px; padding-right:0px; padding-top:5px; padding-bottom:5px
	}        
.articleTools .toolsContainer ul.toolsList li a {
  color: #333;
	}

</style><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><link href="kh_files/main.css" type="text/css" rel="stylesheet"><title>KhoaHoc.com.vn: Cúp vàng Giải thưởng APICTA - Khoa học và Tri thức</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><style type="text/css">
.ad_footer_text a{
font-size:10px;
}
.ad_footer_text a:hover{
font-size:10px;
color:green;
}
</style><script charset="utf-8" id="injection_graph_func" src="kh_files/injection_graph_func.js"></script></head><body bottommargin="0" topmargin="0" leftmargin="5" rightmargin="5" marginheight="0" marginwidth="0">


				<table id="table3" style="border-collapse: collapse;" align="center" border="0" width="990">
					<tbody><tr>
						<td background="kh_files/header_02.gif" height="24" valign="middle">
							<table style="border-collapse: collapse;" valign="middle" border="0" cellspacing="0" width="100%">
								<tbody><tr>
									<td>&nbsp;	
									<b><font face="Verdana" size="1">Useful Links: 
									<a href="http://www.quantrimang.com/">
									<font color="#245edc" face="Verdana" size="1">QuanTriMang.com</font></a> | 
									<a href="http://www.tinthethao.com.vn/">
									<font color="#245edc" face="Verdana" size="1">TinTheThao.com.vn</font></a> | 
									<a href="http://www.hotjobs.com.vn/">
									<font color="#245edc" face="Verdana" size="1">HotJobs.com.vn</font></a> | 
									<a target="_blank" href="http://www.gamevui.com/"><font color="#245edc" face="Verdana" size="1">GameVui.com</font></a>| 
									<a target="_blank" href="http://www.rada.vn/"><font color="#245edc" face="Verdana" size="1">RADA.vn - Nhà đất miễn phí</font></a></font></b></td>
									
									<td align="right"><b>
									<font face="Verdana" size="1">&nbsp;<a href="http://www.khoahoc.com.vn/"><font color="#245edc" face="Verdana" size="1">Trang chủ</font></a> | 
									<a href="http://www.khoahoc.com.vn/members.asp"><font color="#245edc" face="Verdana" size="1">Viết bài</font></a> | 
									<a href="http://www.khoahoc.com.vn/contact.asp"><font color="#245edc" face="Verdana" size="1">Liên hệ</font></a></font></b></td>
								</tr>
							</tbody></table>
					</td></tr>
					<tr>
						<td>
							<table style="border-collapse: collapse;" border="0" cellspacing="0" width="100%">
								<tbody><tr>
									<td>						
                        				<!--<img border="0" src="button/noelve2.gif">-->
                        			    <a href="http://www.khoahoc.com.vn/">
<img src="kh_files/khoahoc.gif" border="0"></a>
</td>
									<td align="right">
 <script type="text/javascript">
	var  ad_header = new MetaNET_SharingAdObject2(
            	0,
            	new Array(
					//new MetaNET_AdObject({'imageUrl':'http://www.quantrimang.com/ads/hoithaonhantai/Nhantai468x60.swf','linkUrl':'http://adrealclick.com/click/?ad=73&cust=hoithaonhantai&pos=66&url=http%3a%2f%2fwww.quantrimang.com%2fphotos%2fFile%2f032008%2fChientranh.html','width':468,'height':60}),
                	//new MetaNET_AdObject({'imageUrl':'http://bongda.com.vn/_self/468x80_022008.gif','linkUrl':'http://www.bongda.com.vn','width':468,'height':60,'wmode':'transparent'}),
					new MetaNET_AdObject({'imageUrl':'http://www.gamevui.com/ads/sms/Galaxy468x60.swf','linkUrl':'#','width':468,'height':60}),
					new MetaNET_AdObject({'imageUrl':'http://www.bongda.com.vn/ad/header/download.com.vn/TEXT.gif','linkUrl':'http://www.download.com.vn','width':468,'height':60})
				)
    );
    ad_header.renderOut();
  </script><div id="MetaNET_SHARING_AD_NUMBER_1"><a href="http://www.download.com.vn/" target="_blank" id="MetaNET_ADV_NUMBER_2"><img src="kh_files/TEXT.gif" border="0" height="60" width="468"></a></div>      
                        			</td>
                        		</tr>    
							</tbody></table>                        		                    		
                        </td>
					</tr>
					<tr>
						<td bgcolor="#839bc3" height="8"></td>
					</tr>
					<tr>
						<td height="3"></td>
					</tr>					
				</tbody></table>			



			 	

	





<script language="JavaScript">
	function OpenWin() 
{
	newwins2 = window.open("","window","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=350,height=350");
	newwins2.focus();
}
	function OpenWinMsg() 
{
	newwins2 = window.open("","window","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=600,height=350");
	newwins2.focus();
}
</script>


<center>
<table style="border-width: 0px;" border="1" bordercolor="#000080" cellpadding="0" cellspacing="0" width="990">
  <tbody><tr> 
    <td style="border-style: none; border-width: medium;" valign="top" width="145">
	
	    

<script language="javascript">


	window.name='lno';
	function mOvr(src) {
		if (!src.contains(event.fromElement)) {
			src.style.cursor = 'hand';
			src.style.backgroundColor = '#FF6600';
		}

	}
	function mOut(src) {
		if (!src.contains(event.toElement)) {
			src.style.cursor = 'default';
			src.style.backgroundColor = '#245EDC';
		}
	}
	function mClk(src) {
		if(event.srcElement.tagName=='TD'){
			src.children.tags('A')[0].click();
		}
	}
	function SmOvr(src) {
		if (!src.contains(event.fromEl+ement)) {
			src.style.cursor = 'hand';
			src.style.backgroundColor = '#FF0000';
		}

	}
	function SmOut(src) {
		if (!src.contains(event.toElement)) {
			src.style.cursor = 'default';
			src.style.backgroundColor = '#306161';
		}
	}
	function SmClk(src) {
		if(event.srcElement.tagName=='TD'){
			src.children.tags('A')[0].click();
		}
	}	

</script>



<table style="border-left-width: 0px; border-right-width: 0px;" border="0" cellpadding="0" cellspacing="0" width="145">	
  <tbody><tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 

		&nbsp;<a href="http://www.khoahoc.com.vn/"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>TRANG CHỦ</b></font></a>
		</td>
	</tr>
 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=12&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>CÔNG NGHỆ MỚI</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=1&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KHOA HỌC VŨ TRỤ</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=2&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KHOA HỌC MÁY TÍNH</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=3&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>PHÁT MINH KHOA HỌC</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=4&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>SINH VẬT HỌC</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=5&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KHẢO CỔ HỌC</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=6&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>Y HỌC - CUỘC SỐNG</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=7&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>MÔI TRƯỜNG</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=8&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>ĐẠI DƯƠNG HỌC</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=9&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>THẾ GIỚI ĐỘNG VẬT</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=10&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>ỨNG DỤNG</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=18&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KHÁM PHÁ</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=15&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>1001 BÍ ẨN</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=13&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>CÂU CHUYỆN KHOA HỌC</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=17&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>CÔNG TRÌNH KHOA HỌC</b></font></a><br>
	</td>
  </tr> 

    
  <tr class="menu">
     <td class="menuright" height="18"> 	 
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=14&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>SỰ KIỆN KHOA HỌC</b></font></a><br>
	</td>
  </tr>
   
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=16&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>THƯ VIỆN ẢNH</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=11&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>GÓC HÀI HƯỚC</b></font></a><br>
	</td>
  </tr> 

 
  <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" height="18" width="165"> 	
     &nbsp;<a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=19&amp;page_id=1"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KHOA HỌC &amp; BẠN ĐỌC</b></font></a><br>
	</td>
  </tr> 


   <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" style="border-bottom-style: none; border-bottom-width: medium;" height="18" width="165"> 	
	&nbsp;<a href="http://www.khoahoc.com.vn/members.asp"><font color="#ffffff" face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>VIẾT BÀI</b></font></a><br>
	</td>
  </tr>
   <tr class="submenu">
     <td class="submenuright" onmouseover="mOvr(this);" onclick="mClk(this);" onmouseout="mOut(this);" style="border-bottom-style: none; border-bottom-width: medium;" height="18" width="165"> 	
	&nbsp;<a href="http://www.khoahoc.com.vn/rss/"><img src="kh_files/rss_feed.png" border="0"></a><br>
	</td>
  </tr>
     
 <tr>
    <td bordercolor="#000080" bgcolor="#839bc3" height="20">
	<p align="center"><b><font color="#ffffff" face="Verdana" size="2">Tìm kiếm</font></b></p></td>
  </tr>  

<!-- SiteSearch Google -->
<form method="get" action="http://www.google.com/custom" target="_top"></form>
 <tr>
    <td style="border-style: none solid solid; border-width: medium 1px 1px;" bordercolor="#000080">&nbsp;
	<div align="center">
<table bgcolor="#ffffff" border="0">
<tbody><tr><td align="left" height="32" nowrap="nowrap" valign="top">
<img src="kh_files/Logo_25wht.gif" alt="Google" border="0">
<br>
<input name="domains" value="KhoaHoc.com.vn" type="hidden">
<input name="q" size="12" maxlength="255" value="" type="text">
</td></tr>
<tr>
<td nowrap="nowrap">
<table>
<tbody><tr>
<td>
<input name="sitesearch" value="KhoaHoc.com.vn" checked="checked" type="radio">
<font color="#000000" size="-1">KhoaHoc.com.vn</font>
</td>
</tr>
</tbody></table>
<input name="sa" value="Tìm kiếm" type="submit">
<input name="client" value="pub-7201372296576994" type="hidden">
<input name="forid" value="1" type="hidden">
<input name="ie" value="UTF-8" type="hidden">
<input name="oe" value="UTF-8" type="hidden">
<input name="cof" value="GALT:#008000;GL:1;DIV:#336699;VLC:663399;AH:center;BGC:FFFFFF;LBGC:0055E6;ALC:0000FF;LC:0000FF;T:000000;GFNT:0000FF;GIMP:0000FF;LH:50;LW:760;L:http://www.khoahoc.com.vn/button/khoahoc_google.gif;S:http://www.khoahoc.com.vn;FORID:1;" type="hidden">
<input name="hl" value="en" type="hidden">

</td></tr></tbody></table>
	</div>
	</td>
  </tr>

<!-- SiteSearch Google -->
<tr>
	<td>
<center>
<script type="text/javascript"><!--
google_ad_client = "pub-7201372296576994";
/* 120x600, Khoahoc */
google_ad_slot = "7605814646";
google_ad_width = 120;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript" src="kh_files/show_ads.js">
</script><iframe name="google_ads_frame" src="kh_files/ads.htm" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" frameborder="0" height="600" scrolling="no" width="120"></iframe>
</center>
</td>
</tr>

   
 
 </tbody></table>

	</td>
    <td style="border-style: none; border-width: medium;" align="center" height="360" valign="top" width="5"> 
      </td>
    <td style="border-style: none; border-width: medium;" align="left" valign="top"> 
      <b><a href="http://www.khoahoc.com.vn/">TRANG CHỦ</a> <font face="Verdana" size="2">::&gt;&gt;</font> <a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=14">SỰ KIỆN KHOA HỌC</a></b> 
<hr color="#839bc3" size="1">


	
        
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
 
 <tbody><tr> 
          <td align="center" valign="top"> 
<table align="right" bgcolor="#ffffff" border="0" cellpadding="5" cellspacing="0" width="300">
	<tbody><tr><td style="height: 250px;">


<script type="text/javascript">
    var  ad_view =  new MetaNET_SharingAdObject2(
            	0,
            	new Array(
					//new MetaNET_AdObject({'imageUrl':'/_self/chuyenmuc/1001bian.swf','linkUrl':'#','width':300,'height':250}),
					new MetaNET_AdObject({'imageUrl':'http://www.gamevui.com/ads/fly_spider300x250.swf','linkUrl':'#','width':300,'height':250})
				)
    );
    ad_view.renderOut();
</script><div id="MetaNET_SHARING_AD_NUMBER_2"><div style="width: 300px; height: 250px;" id="MetaNET_ADV_NUMBER_3"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" id="MetaNET_ADV_NUMBER_3_FLASH" align="middle" height="250" width="300"> <param name="allowScriptAccess" value="sameDomain"> <param name="movie" value="http://www.gamevui.com/ads/fly_spider300x250.swf"> <param name="wmode" value="window"> <param name="linkUrl" value="#"> <param name="quality" value="high"><embed src="kh_files/fly_spider300x250.swf" quality="high" id="MetaNET_ADV_NUMBER_3_FLASH" name="MetaNET_ADV_NUMBER_3_FLASH" allowscriptaccess="sameDomain" type="application/x-shockwave-flash" wmode="window" linkurl="#" pluginspage="http://www.macromedia.com/go/getflashplayer" align="middle" height="250" width="300"> </object></div></div> 
<div style="padding: 3px; background-color: rgb(255, 255, 255); font-weight: bold;"></div>
<script type="text/javascript"><!--
google_ad_client = "pub-7201372296576994";
/* Khoahoc_300x250 */
google_ad_slot = "3804096563";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript" src="kh_files/show_ads.js">
</script><iframe name="google_ads_frame" src="kh_files/ads_002.htm" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" frameborder="0" height="250" scrolling="no" width="300"></iframe> 
	</td></tr>
	<tr><td>
	
	<div style="padding: 5px; background-color: rgb(218, 218, 218); font-weight: bold;">
				 Các bài cũ hơn cùng chủ đề: </div>
				<ul style="margin: 0px; padding-left: 18px;">
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/19/2006 11:36:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3524">Dành 100 tỷ đồng xây dựng 15 khu bảo tồn biển</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/19/2006 11:19:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3523">Nhà khoa học sẽ có tiền, có quyền</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/18/2006 8:22:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3510">Năm 2020, VN sẽ có nhà máy điện hạt nhân đầu tiên</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/17/2006 5:18:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3492">Vườn ươm cho bảo tàng lịch sử tự nhiên đầu tiên của Việt Nam</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/17/2006 5:02:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3488">Cóc khổng lồ tràn ngập Australia</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/16/2006 3:55:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3469">Sắp có loại vũ khí sinh học mới</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/16/2006 3:46:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3468">Thêm một scandal ngụy tạo khoa học</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/16/2006 3:38:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3466">100 tỷ đồng để phát triển các khu bảo tồn biển</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/15/2006 4:15:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3431">Dự báo những nghiên cứu đột phá năm 2006</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/15/2006 4:06:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3430">Thiết lập hệ thống quản lý thiên tai tại châu Á</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/13/2006 8:19:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3398">Thịt cá voi 'làm thức ăn cho chó'</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/13/2006 4:07:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3392">Tìm ra hóa chất ngăn nhặn HIV phát triển</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/12/2006 9:55:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3381">Chàng sinh viên và dưa hấu hình... kim tự tháp</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/12/2006 9:33:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3378">Nông dân biến năng lượng mặt trời thành điện</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/11/2006 4:59:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3363">Trung Quốc: Mở cửa viện bảo tàng hạt nhân</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/11/2006 4:49:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3361">NASA: Các thách thức thiên niên kỷ</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/11/2006 4:35:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3358">WWF cảnh báo sự thu hẹp dần diện tích các vùng đất ngập nước</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/11/2006 4:32:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3357">Sắp có Viện Khoa học công nghệ tính toán</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/10/2006 3:22:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3345">Trưng bày bản đồ cổ nhất phương Tây được vẽ trên giấy papyrus</a> </li>
		  

	<li style="margin-left: 0px; padding-left: 0px; list-style-type: circle; list-style-image: none; list-style-position: outside;"> 
	 <a style="" title="Cập nhật ngày 2/9/2006 4:48:00 AM" href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;news_id=3318">2005: VN chỉ có 1 đơn đăng ký cấp bằng sáng chế!</a> </li>
</ul>
<div align="right"><a href="http://www.khoahoc.com.vn/details.asp?Cat_ID=14&amp;Cat_Sub_ID=0"><b><font color="#ff0000" face="Arial" size="2">Xem toàn bộ &gt;&gt;</font></b></a>	
</div>
 
	</td></tr>
</tbody></table>
<div align="left">
			  <span class="bodytxt">
	<b><font color="#0033cc" face="verdana" size="4">Cúp vàng Giải thưởng APICTA</font></b><font size="2"> - 20/2/2006 11h:45</font>

	<p align="justify">


	</p><p dir="ltr" style="margin-right: 0px;" align="center"><strong>Sản phẩm “<em>Từ điển ký hiệu giao tiếp của người khiếm thính</em>” <br>của Việt Nam đoạt cúp vàng Giải thưởng Công nghệ thông tin - Truyền thông <br>Châu Á - Thái Bình Dương (APICTA)</strong></p>
<p align="justify">Tối ngày 19/02/2006 tại hội trường KS.Khum Khantoke
giữa trung tâm thành phố Chiang Mai - Thái Lan, Lễ trao giải thưởng
công nghệ thông tin - truyền thông (CNTT-TT) Châu Á - Thái Bình Dương
(APICTA - Asia Pacific ICT Awards) lần thứ 5 được tổ chức trọng thể.
Tham dự buổi lễ có Ngài Kanawat Vasinsungworn Thứ Trưởng bộ Công nghệ
thông tin - Viễn thông Thái Lan. Đây là giải thưởng thường niên nhằm
xem xét chất lượng và kết quả sản phẩm CNTT ở khu vực và quốc tế, ngôn
ngữ của APICTA bằng tiếng Anh. Các thành viên tham dự APICTA 2005 bao
gồm: Việt Nam, Singapore, Pakistan, Sri Lanka, Hongkong, Indonesia,
Thái Lan, Ma Cao, Malaysia, Úc, Brunei với 164 sản phẩm dự thi. <br><br>Giải
thưởng CNTT-TT Khu vực Châu Á - Thái Bình Dương được lập theo sáng kiến
của Nguyên Thủ tướng Malaysia ngài Dato Mahathir Mohamat, năm 2001 và
năm 2002 được tổ chức tại Malaysia, năm 2003 tại Thái Lan, 2004 tại
Hồng Kông, hàng năm luân phiên tại các nước thành viên. <br><br>APICTA
có tầm quan trọng đặc biệt, góp phần tăng cường giao lưu, thúc đẩy hợp
tác phát triển Công nghiệp CNTT-TT khu vực Châu Á - Thái Bình Dương.
Tính chất của giải là thi đấu và cạnh tranh. Không chỉ đơn thuần là
cuộc cạnh tranh giữa các doanh nghiệp mà còn là cạnh tranh toàn diện
giữa các quốc gia về trình độ Kỹ thuật, Khoa học và Công nghệ, Nghiên
cứu, Đào tạo và ứng dụng CNTT-TT. Thông qua APICTA có thể thấy rõ nét
mức độ phát triển CNTT-TT của các nước. APICTA thực sự là một giải
thưởng danh giá nhất về CNTT-TT Khu vực Châu Á Thái Bình Dương hiện
nay. <br><br>Do sự phát triển về phạm vi ứng dụng CNTT, giải năm nay
được mở rộng từ 14 lên 15 chuyên ngành cho các sản phẩm ở trình độ
chuyên nghiệp của Công nghiệp CNTT, dự án học sinh - sinh viên; trong
đó có 12 chuyên ngành với các sản phẩm ở trình độ chuyên nghiệp của
Công nghiệp CNTT-TT, ứng dụng trên các lĩnh vực: Giáo dục đào tạo; Y
tế; An toàn thông tin; Chính phủ điện tử và các dịch vụ công; Các công
cụ ứng dụng và Cơ sở hạ tầng; Công nghiệp; Các ứng dụng đóng gói; Tài
chính; Du lịch; Viễn thông; Công nghiệp giải trí; Khởi tạo doanh nghiệp
và 3 loại chuyên ngành cho lĩnh vực nghiên cứu phát triển và sinh viên
là: Dự án Nghiên cứu phát triển (R&amp;D) CNTT-TT; Dự án CNTT-TT Sinh
viên đại học; Dự án CNTT-TT Học sinh phổ thông. <br><br>Tham gia
APICTA 2005, Đoàn Việt Nam với 25 thành viên do Tiến sĩ Nguyễn Quý Sơn,
Chủ tịch APICTA Việt Nam, dẫn đầu tham dự với 9 sản phẩm, trong các
chuyên ngành sau: <br><br>- Chuyên ngành Chính phủ điện tử và dịch vụ công là lãnh vực lần đầu tiên Việt Nam tham gia với sản phẩm “<strong><em>Hệ thống văn phòng điện tử - E-Office System</em></strong>” của Sở khoa học và Công nghệ Đồng Nai; và sản phẩm “<strong><em>Hệ thống quản trị thông tin doanh nghiệp - The Business Information Management System (BIMS)</em></strong>” của Công ty FPT. <br><br>- Chuyên ngành Tài chính với sản phẩm “<strong><em>Ngân hàng thông minh – Smart Bank</em></strong>” của Công ty FPT. <br><br>- Chuyên ngành Các ứng dụng đóng gói với sản phẩm “<strong><em>Giải pháp hoạch định nguồn lực doanh nghiệp - IRP Solution an ERP Packaged</em></strong>” của Công ty AZ Solution JSC. <br><br>- Chuyên ngành nghiên cứu và phát triển với sản phẩm “<strong><em>Hệ thông giám sát giao thông bằng máy tính - Traffic Monitoring System by Computer Vision</em></strong>” của Liên hiệp khoa học - sản xuất – công nghệ mới (NEWSTECPRO), <br><br>- Chuyên ngành thuộc dự án của sinh viên Đại học với Sản phẩm “<strong><em>Giải pháp chuẩn hóa và làm sạch mạng trong suốt lớp TCP\IP - Transparent Network and Transport Layer Nomilizer</em></strong>” của Viện Tin học ứng dụng; và sản phẩm “<strong><em>Hệ thống thông tin y tế ở khắp mọi nơi - Ubiquitous Healthcare Information System</em></strong>” của Đại học ngoại thương Hà Nội, <br><br>- Chuyên ngành Giáo dục và Đào tạo với sản phẩm “<strong><em>Từ điển ký hiệu giao tiếp của người khiếm thính</em></strong>” của Đại học Sư phạm Tp.HCM, <br><br>- Chuyên ngành Khởi tạo doanh nghiệp với sản phẩm “<strong><em>Cổng
thông tin tích hợp Hệ thống diễn đàn và tòa soạn điện tử - The online
Portal- integrated News Providing Solution: mvnForum and mvnCMS</em></strong>” của Công ty Hữu Ngọc (nhóm MVN) <br><br>Theo
đánh giá chung của Ban giám khảo và các nhà chuyên môn, sản phẩm dự thi
năm nay có trình độ công nghệ và chuyên nghiệp cao hơn nhiều so với các
năm trước đây. <br><br>Qua 2 ngày tổ chức thi với phong cách tổ chức
chuyên nghiệp và sự làm việc rất công minh, công bằng của Ban Giám
khảo, tối 19/2 Ban Tổ chức giải đã ra Quyết định công bố trao 14 cúp
vàng, 12 cúp bạc và 02 cúp đồng cho các Quốc gia và nền kinh tế: </p>
<p align="justify">
<table align="center" border="1" cellpadding="5" cellspacing="1" width="95%">
    <tbody>
        <tr>
            <td bgcolor="#006699">
            <p align="center"><strong><font color="#ffffff">STT</font></strong></p>
            </td>
            <td bgcolor="#006699">
            <p align="center"><strong><font color="#ffffff">Quốc gia và nền kinh tế</font></strong></p>
            </td>
            <td bgcolor="#006699">
            <p align="center"><strong><font color="#ffffff">Vàng</font></strong></p>
            </td>
            <td bgcolor="#006699">
            <p align="center"><strong><font color="#ffffff">Bạc</font></strong></p>
            </td>
            <td bgcolor="#006699">
            <p align="center"><strong><font color="#ffffff">Đồng</font></strong></p>
            </td>
        </tr>
        <tr>
            <td>
            <p align="center">1</p>
            </td>
            <td>
            <p align="left">Hồng Kông</p>
            </td>
            <td>
            <p align="center">06</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td>
            <p align="center">2</p>
            </td>
            <td>
            <p align="left">Malaysia</p>
            </td>
            <td>
            <p align="center">03</p>
            </td>
            <td>
            <p align="center">04</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td>
            <p align="center">3</p>
            </td>
            <td>
            <p align="left">Thái Lan</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
            <td>
            <p align="center">03</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td>
            <p align="center">4</p>
            </td>
            <td>
            <p align="left">Singapore</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
            <td>
            <p align="center">02</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td>
            <p align="center">5</p>
            </td>
            <td>
            <p align="left">Úc</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
        </tr>
        <tr>
            <td>
            <p align="center">6</p>
            </td>
            <td>
            <p align="left">Việt Nam</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td>
            <p align="center">7</p>
            </td>
            <td>
            <p align="left">Pakistan</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td>
            <p align="center">8</p>
            </td>
            <td>
            <p align="left">Sri Lanka</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td>
            <p align="center">9</p>
            </td>
            <td>
            <p align="left">Ma Cao</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
            <td>
            <p align="center">&nbsp;</p>
            </td>
            <td>
            <p align="center">01</p>
            </td>
        </tr>
    </tbody>
</table>
<br>Các nước không đoạt giải trong giải thưởng lần này: Indonesia, Brunei. <br><br>Trong đó, Việt nam có sản phẩm “<em>Từ điển ký hiệu giao tiếp của người khiếm thính</em>” của Đại học Sư phạm Tp.HCM là sản phẩm đạt giải nhất trong lĩnh vực Giáo dục – Đào tạo, được tặng thưởng cúp vàng. Sản phẩm “<em>Từ điển ký hiệu giao tiếp của người khiếm thính</em>”
ứng dụng công nghệ lập trình Dot Net để tạo ra một từ điển điện tử với
3900 mẫu minh họa cho 2000 từ thông dụng cho phép người dùng có thể tự
học ký hiệu giao tiếp. Với bộ từ điển này, học sinh khiếm thính có thể
dùng máy vi tính để tự trau dồi môn học ký hiệu ngoài những giờ học với
giáo viên tại lớp. Các giáo viên dạy trẻ khiếm thính và sinh viên ngành
giáo dục đặc biệt cũng dùng bộ từ điển này để trau dồi kỹ năng về ký
hiệu để có thể giao tiếp với học sinh của mình, tăng hiệu quả giảng
dạy. Đồng thời, những người có thân nhân bị khiếm thính cũng có thể học
ký hiệu từ bộ từ điển này để giao tiếp với thân nhân của mình, giúp thu
ngắn khoảng cách giao tiếp giữa người khiếm thính và gia đình của họ,
tạo điều kiện cho người khiếm thính có nhiều cơ hội hòa nhập cộng đồng.
<br><br>Đặc điểm của phần mềm này là tính năng dữ liệu mở, cho phép
người dùng có thể tự cập nhật từ điển theo ngôn ngữ riêng của mình. Do
đó, từ điển này không chỉ áp dụng cho Việt Nam mà còn cho bất kỳ nước
nào có nhu cầu xây dựng một từ điển dành cho người khiếm thính. <br><br>Một
đặc điểm nữa là từ điển này đã và đang được phân phát miễn phí cho các
trường và phụ huynh có nhu cầu. Trong tương lai sẽ được mở mã nguồn để
mọi người tham khảo, hiệu chỉnh cho phù hợp với yêu cầu của mình. <br><br>Sản
phẩm E-Office của Đồng Nai tuy không đoạt giải nhưng đã được đánh giá
cao trong ứng dụng thuộc lĩnh vực Chính phủ điện tử và dịch vụ công <br><br>Kết
thúc buổi lễ với các hoạt động thắm tình đoàn kết và hữu nghị giữa các
nước, tại đại sảnh của khách sạn Khum Khantoke đã diễn ra buổi giao lưu
thân mật giữa các quốc gia tham dự giải thưởng. <br><br>Với quy mô và
tầm vóc của APICTA và lợi ích của quốc gia đăng cai về nhiều mặt như:
những tri thức, ý tưởng và công nghệ mới về ứng dụng CNTT-TT được tập
hợp về Ban tổ chức, quảng bá hình ảnh đất nước, tạo lập thị trường ….
Do đó, nhiều nước đang mong muốn được đăng cai tổ chức APICTA hàng năm;
Thiết nghĩ Việt Nam cũng nên có kế hoạch chuẩn bị để tiến hành đăng cai
tổ chức APICTA nhằm tăng cường giao lưu, hợp tác, thúc đẩy phát triển
Công nghiệp CNTT-TT của Việt Nam trong tương lai. <br><br>Tin và phim của Xuân Trường – Minh Hòa, Sở Khoa học và Công nghệ Đồng Nai chuyển từ Chiang Mai về Việt Nam <br><br>
<table align="center" border="0" cellpadding="3" cellspacing="1" width="200">
    <tbody>
        <tr>
            <td>&nbsp;<img src="kh_files/DSCF5292.jpg" alt=""></td>
        </tr>
        <tr>
            <td>
            <p align="center"><font color="#3366ff"><strong>Hình DSCF5292:</strong> Thứ trưởng Bộ CNTT-VT Thái Lan phát biểu tại buổi lễ<br></font></p>
            </td>
        </tr>
        <tr>
            <td>&nbsp;<img src="kh_files/DSC00985.jpg" alt=""></td>
        </tr>
        <tr>
            <td>
            <p align="center"><font color="#3366ff"><strong>Hình DSC00985:</strong> Trao cúp vàng cho sản phẩm của Việt Nam </font></p>
            </td>
        </tr>
    </tbody>
</table>
<br>Nội dung bài đã được Ông Nguyễn Quý Sơn - Trưởng đoàn Việt Nam thông qua </p>

<br>
<b><i></i></b>
	<p></p>
<div align="left">
<table id="table1" style="border-collapse: collapse;" border="0" bordercolor="#0066ff" width="468">
	<tbody><tr>
		<td>

<script type="text/javascript"><!--
google_ad_client = "pub-7201372296576994";
/* 468x60, created 3/16/08 */
google_ad_slot = "6767064661";
google_ad_width = 468;
google_ad_height = 60;
//-->
</script>
<script type="text/javascript" src="kh_files/show_ads.js">
</script><iframe name="google_ads_frame" src="kh_files/ads_003.htm" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" frameborder="0" height="60" scrolling="no" width="468"></iframe>
		</td>
	</tr>
</tbody></table>

<br> Tìm kiếm thêm thông tin Khoa Học:
<!-- SiteSearch Google -->
<form method="get" action="http://www.google.com.vn/custom" target="_top">
<table bgcolor="#ffffff" border="0">
<tbody><tr><td align="left" height="32" nowrap="nowrap" valign="top">
<a href="http://www.google.com/">
<img src="kh_files/Logo_25wht.gif" alt="Google" align="middle" border="0"></a>
</td>
<td nowrap="nowrap">
<input name="domains" value="www.khoahoc.com.vn" type="hidden">
<label for="sbi" style="display: none;">Nhập các thuật ngữ tìm kiếm của bạn</label>
<input name="q" size="31" maxlength="255" value="" id="sbi" type="text">
<label for="sbb" style="display: none;">Nộp mẫu đơn tìm kiếm</label>
<input name="sa" value="Tìm kiếm" id="sbb" type="submit">
</td></tr>
<tr>
<td>&nbsp;</td>
<td nowrap="nowrap">
<table>
<tbody><tr>
<td>
<input name="sitesearch" value="" id="ss0" type="radio">
<label for="ss0" title="Tìm Kiếm trên Web"><font color="#000000" size="-1">Web</font></label></td>
<td>
<input name="sitesearch" value="www.khoahoc.com.vn" checked="checked" id="ss1" type="radio">
<label for="ss1" title="Tìm kiếm www.khoahoc.com.vn"><font color="#000000" size="-1">www.khoahoc.com.vn</font></label></td>
</tr>
</tbody></table>
<input name="client" value="pub-7201372296576994" type="hidden">
<input name="forid" value="1" type="hidden">
<input name="channel" value="6948982507" type="hidden">
<input name="ie" value="UTF-8" type="hidden">
<input name="oe" value="UTF-8" type="hidden">
<input name="cof" value="GALT:#008000;GL:1;DIV:#336699;VLC:663399;AH:center;BGC:FFFFFF;LBGC:0055E6;ALC:0000FF;LC:0000FF;T:000000;GFNT:0000FF;GIMP:0000FF;LH:50;LW:760;L:http://www.khoahoc.com.vn/button/khoahoc_google.gif;S:http://www.khoahoc.com.vn;FORID:1" type="hidden">
<input name="hl" value="vi" type="hidden">
</td></tr></tbody></table>
</form>
<!-- SiteSearch Google -->
</div>	
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr>
    <td>&nbsp;<a href="http://www.khoahoc.com.vn/pop_print.asp?news_id=3555" onclick="OpenWinMsg()" target="window"><img src="kh_files/print_icon.gif" border="0"> 
	In bài này</a> | 
	<img src="kh_files/star.gif" border="0" height="18" width="30"><font color="#ff0000" face="verdana" size="2"><b><a href="http://www.khoahoc.com.vn/send_to_us.asp?Cat_ID=14&amp;N_ID=3555">Trao đổi</a></b></font> | 
	<img src="kh_files/email.gif" border="0" height="18" width="30"><a href="http://www.khoahoc.com.vn/send_to_friend.asp?Cat_ID=14&amp;N_ID=3555">Gửi cho bạn bè</a></td>
    <td align="right">&nbsp;</td>
  </tr>
</tbody></table>
<br>			
			
<hr color="#839bc3" size="1"><b>Các bài mới nhất: </b> &lt;<a href="http://www.khoahoc.com.vn/new.asp?page_id=1">Xem tất cả</a>&gt;<br>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody><tr>
    <td>
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=13&amp;Cat_Sub_ID=0&amp;news_id=19929">
	<font face="Times New Roman" size="3">Người làm cách mạng trong vật lý</font></a>
	<font color="#000000" size="2"> - 23/4</font>

 <b><font color="#999999" face="Verdana" size="1"><img src="kh_files/icon_new1.jpg" border="1"></font></b>	

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=2&amp;Cat_Sub_ID=7&amp;news_id=19928">
	<font face="Times New Roman" size="3">Google là thương hiệu mạnh nhất với 86 tỉ USD</font></a>
	<font color="#000000" size="2"> - 23/4</font>

 <b><font color="#999999" face="Verdana" size="1"><img src="kh_files/icon_new1.jpg" border="1"></font></b>	

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=6&amp;Cat_Sub_ID=5&amp;news_id=19926">
	<font face="Times New Roman" size="3">Mẹ ăn nhiều calo dễ sinh con trai</font></a>
	<font color="#000000" size="2"> - 23/4</font>

 <b><font color="#999999" face="Verdana" size="1"><img src="kh_files/icon_new1.jpg" border="1"></font></b>	

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=9&amp;Cat_Sub_ID=0&amp;news_id=19925">
	<font face="Times New Roman" size="3">Môi trường sống mới khiến thằn lằn tiến hóa nhanh hơn</font></a>
	<font color="#000000" size="2"> - 23/4</font>

 <b><font color="#999999" face="Verdana" size="1"><img src="kh_files/icon_new1.jpg" border="1"></font></b>	

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=14&amp;Cat_Sub_ID=0&amp;news_id=19927">
	<font face="Times New Roman" size="3">Mỹ Latin cảnh báo tác động sản xuất nhiên liệu sinh học</font></a>
	<font color="#000000" size="2"> - 23/4</font>

 <b><font color="#999999" face="Verdana" size="1"><img src="kh_files/icon_new1.jpg" border="1"></font></b>	

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=6&amp;Cat_Sub_ID=5&amp;news_id=19923">
	<font face="Times New Roman" size="3">Thiếu ngủ có thể gây ra chứng mộng du</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=7&amp;Cat_Sub_ID=2&amp;news_id=19922">
	<font face="Times New Roman" size="3">Chế tạo loại nhựa thân thiện môi trường</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=4&amp;Cat_Sub_ID=0&amp;news_id=19920">
	<font face="Times New Roman" size="3">Bộ não của con đực và con cái không khác nhau là mấy</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=6&amp;Cat_Sub_ID=0&amp;news_id=19918">
	<font face="Times New Roman" size="3">Có thể chẩn đoán bệnh tim nhờ nước bọt</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=9&amp;Cat_Sub_ID=0&amp;news_id=19917">
	<font face="Times New Roman" size="3">Cú đớp của rồng Komodo yếu hơn cả... mèo nhà</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=2&amp;Cat_Sub_ID=7&amp;news_id=19916">
	<font face="Times New Roman" size="3">Ebay cân nhắc bán Skype</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=13&amp;Cat_Sub_ID=0&amp;news_id=19915">
	<font face="Times New Roman" size="3">Từ lò hạt nhân tự nhiên đến vũ trụ song hành!</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=4&amp;Cat_Sub_ID=12&amp;news_id=19914">
	<font face="Times New Roman" size="3">Giải mã quá trình phân chia của vi khuẩn</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=4&amp;Cat_Sub_ID=0&amp;news_id=19913">
	<font face="Times New Roman" size="3">Côn trùng tiến hóa một chiến thuật hoàn toàn khác để đánh hơi</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=2&amp;Cat_Sub_ID=7&amp;news_id=19912">
	<font face="Times New Roman" size="3">Trình duyệt không an toàn bị loại khỏi Paypal</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=5&amp;Cat_Sub_ID=0&amp;news_id=19911">
	<font face="Times New Roman" size="3">Đội quân đất nung của Tần Thủy Hoàng được "sơn" bằng trứng</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=5&amp;Cat_Sub_ID=0&amp;news_id=19910">
	<font face="Times New Roman" size="3">Phát hiện kho báu Viking ở Thụy Điển</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=18&amp;Cat_Sub_ID=0&amp;news_id=19908">
	<font face="Times New Roman" size="3">Ra quyết định có thể là một hoạt động vô thức</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=15&amp;Cat_Sub_ID=0&amp;news_id=19921">
	<font face="Times New Roman" size="3">Kinh ngạc chuyện đầu thai</font></a>
	<font color="#000000" size="2"> - 22/4</font>

	<br>
   				
		  


	<a href="http://www.khoahoc.com.vn/view.asp?Cat_ID=9&amp;Cat_Sub_ID=0&amp;news_id=19907">
	<font face="Times New Roman" size="3">Quan hệ phối giống trong dòng họ</font></a>
	<font color="#000000" size="2"> - 21/4</font>

	<br>
   				

</td>

</tr>
</tbody></table>

<p align="right">
			<a href="http://www.khoahoc.com.vn/new.asp?page_id=1"><b><font color="#0000ff" face="Arial" size="2">Xem toàn bộ &gt;&gt;</font></b></a>
			<br>
<a href="#top" onmouseover="(window.status='Trở về đầu trang...'); return true" onmouseout="(window.status=''); return true" tabindex="-1">
	Trở về đầu trang <img src="kh_files/top.gif" alt="Tro ve dau trang" border="0"></a>
</p>

</span></div></td>
        </tr>
      </tbody></table> 
  </td></tr>
</tbody></table>


<center>
<table style="border-collapse: collapse;" border="0" cellpadding="0" height="56" width="990">
		<tbody><tr>
			<td align="center" height="5" width="5">

			</td>
		</tr>	
		<tr>
			<td background="kh_files/bg_table_footer.gif" height="25">
			<p align="left"><b><font color="#ffffff" face="Verdana" size="1">&nbsp;&nbsp; 
			<a href="http://www.khoahoc.com.vn/"><font color="#ffffff" face="Verdana" size="1">Trang 
			chủ</font></a> | <a href="http://www.khoahoc.com.vn/members.asp"><font color="#ffffff" face="Verdana" size="1">Gửi bài viết</font></a> | 
			<a href="http://www.khoahoc.com.vn/contact.asp"><font color="#ffffff" face="Verdana" size="1">Góp ý - Liên hệ</font></a></font></b></p></td>
		</tr>
		<tr>
			<td height="5" width="5">
			</td>
		</tr>		
		<tr>
			<td height="26" valign="top">
			<table id="table1" style="border-collapse: collapse;" border="0" width="100%">
				<tbody><tr>
					<td width="50%">

<div class="ad_footer_text" align="left"><font face="Verdana" size="1">
 <img src="kh_files/t-95020-127898-132538-124759-132539.gif" height="1" width="1"><a target="_blank" href="http://www.modchipman.com/xecuter-ce-p-166.html?ref=9">Xbox Mod Chips</a> | 
<a target="_blank" href="http://threestore.three.co.uk/">Mobile Phones</a> | 
<a target="_blank" href="http://www.moneyweb.co.uk/products/mortgages/mcapital.html">Mortgage Calculator</a> | 
<a target="_blank" href="http://www.1stdirectmortgages.co.uk/">Mortgages</a> | 
<a target="_blank" href="http://www.moneyweb.co.uk/products/mortgages/mcapital.html">Mortgages</a><br>

</font></div>						
					
					</td>
					<td>
					<p align="right">
					<font color="#666666" face="Verdana" size="1">Copyright © 
					2004-2007 </font>
					<font color="#666666" face="Verdana"> <b><a href="http://www.khoahoc.com.vn/">
					<font color="#666666" size="1">KhoaHoc.com.vn</font></a></b></font><font color="#666666" face="Verdana" size="1">. All rights reverved<br>
					Designed and Developed by </font>
					<font color="#666666" face="Verdana"> <b>
					<a href="http://www.khoahoc.com.vn/">
					<font color="#666666" size="1">KhoaHoc.com.vn</font></a></b></font></p></td>
				</tr>
			</tbody></table>
			</td>
		</tr>
		<tr>
			<td height="5" width="5">
			</td>
		</tr>		
</tbody></table>

</center>
<iframe src="kh_files/ranking.htm" frameborder="0" height="1" scrolling="no" width="1"></iframe>
<iframe src="kh_files/ranking_002.htm" frameborder="0" height="1" scrolling="no" width="1"></iframe>
<script type="text/javascript" src="kh_files/ie_flash_fix.js"></script>


</center>
</body><script type="text/javascript"><!--
function __RP_Callback_Helper(str, strCallbackEvent, splitSize, func){var event = null;if (strCallbackEvent){event = document.createEvent('Events');event.initEvent(strCallbackEvent, true, true);}if (str && str.length > 0){var splitList = str.split('|');var strCompare = str;if (splitList.length == splitSize)strCompare = splitList[splitSize-1];var pluginList = document.plugins;for (var count = 0; count < pluginList.length; count++){var sSrc = '';if (pluginList[count] && pluginList[count].src)sSrc = pluginList[count].src;if (strCompare.length >= sSrc.length){if (strCompare.indexOf(sSrc) != -1){func(str, count, pluginList, splitList);break;}}}}if (strCallbackEvent)document.body.dispatchEvent(event);}function __RP_Coord_Callback(str){var func = function(str, index, pluginList, splitList){pluginList[index].__RP_Coord_Callback = str;pluginList[index].__RP_Coord_Callback_Left = splitList[0];pluginList[index].__RP_Coord_Callback_Top = splitList[1];pluginList[index].__RP_Coord_Callback_Right = splitList[2];pluginList[index].__RP_Coord_Callback_Bottom = splitList[3];};__RP_Callback_Helper(str, 'rp-js-coord-callback', 5, func);}function __RP_Url_Callback(str){var func = function(str, index, pluginList, splitList){pluginList[index].__RP_Url_Callback = str;pluginList[index].__RP_Url_Callback_Vid = splitList[0];pluginList[index].__RP_Url_Callback_Parent = splitList[1];};__RP_Callback_Helper(str, 'rp-js-url-callback', 3, func);}function __RP_TotalBytes_Callback(str){var func = function(str, index, pluginList, splitList){pluginList[index].__RP_TotalBytes_Callback = str;pluginList[index].__RP_TotalBytes_Callback_Bytes = splitList[0];};__RP_Callback_Helper(str, null, 2, func);}function __RP_Connection_Callback(str){var func = function(str, index, pluginList, splitList){pluginList[index].__RP_Connection_Callback = str;pluginList[index].__RP_Connection_Callback_Url = splitList[0];};__RP_Callback_Helper(str, null, 2, func);}
//--></script></html>