// **********************************************************************
//
// Generated by the ORBacus IDL to Java Translator
//
// Copyright (c) 2005
// IONA Technologies, Inc.
// Waltham, MA, USA
//
// All Rights Reserved
//
// **********************************************************************

// Version: 4.3.2

package CorbaObject.DispatcherPackage;

//
// IDL:CorbaObject/Dispatcher/DataFinale:1.0
//
final public class DataFinaleHelper
{
    public static void
    insert(org.omg.CORBA.Any any, DataFinale val)
    {
        org.omg.CORBA.portable.OutputStream out = any.create_output_stream();
        write(out, val);
        any.read_value(out.create_input_stream(), type());
    }

    public static DataFinale
    extract(org.omg.CORBA.Any any)
    {
        if(any.type().equivalent(type()))
            return read(any.create_input_stream());
        else
            throw new org.omg.CORBA.BAD_OPERATION();
    }

    private static org.omg.CORBA.TypeCode typeCode_;

    public static org.omg.CORBA.TypeCode
    type()
    {
        if(typeCode_ == null)
        {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            org.omg.CORBA.StructMember[] members = new org.omg.CORBA.StructMember[1];

            members[0] = new org.omg.CORBA.StructMember();
            members[0].name = "data";
            members[0].type = octetListHelper.type();

            typeCode_ = orb.create_struct_tc(id(), "DataFinale", members);
        }

        return typeCode_;
    }

    public static String
    id()
    {
        return "IDL:CorbaObject/Dispatcher/DataFinale:1.0";
    }

    public static DataFinale
    read(org.omg.CORBA.portable.InputStream in)
    {
        DataFinale _ob_v = new DataFinale();
        _ob_v.data = octetListHelper.read(in);
        return _ob_v;
    }

    public static void
    write(org.omg.CORBA.portable.OutputStream out, DataFinale val)
    {
        octetListHelper.write(out, val.data);
    }
}
