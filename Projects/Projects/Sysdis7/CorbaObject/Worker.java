// **********************************************************************
//
// Generated by the ORBacus IDL to Java Translator
//
// Copyright (c) 2005
// IONA Technologies, Inc.
// Waltham, MA, USA
//
// All Rights Reserved
//
// **********************************************************************

// Version: 4.3.2

package CorbaObject;

//
// IDL:CorbaObject/Worker:1.0
//
/***/

public interface Worker extends WorkerOperations,
                                org.omg.CORBA.Object,
                                org.omg.CORBA.portable.IDLEntity
{
}
