\contentsline {chapter}{Abstract}{1}{section*.1}
\contentsline {chapter}{Acknowledgements}{3}{chapter*.3}
\contentsline {chapter}{Table of contents}{5}{chapter*.4}
\contentsline {chapter}{List of Figures}{6}{chapter*.5}
\contentsline {chapter}{List of Tables}{7}{chapter*.6}
\contentsline {chapter}{List of Algorithms}{8}{chapter*.7}
\contentsline {chapter}{\numberline {1}Introduction}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Context}{9}{section.1.1}
\contentsline {section}{\numberline {1.2}Goal }{10}{section.1.2}
\contentsline {section}{\numberline {1.3}Approach}{10}{section.1.3}
\contentsline {section}{\numberline {1.4}Evaluation methods}{11}{section.1.4}
\contentsline {section}{\numberline {1.5}Structure of the report}{12}{section.1.5}
\contentsline {chapter}{\numberline {2}Related Work}{13}{chapter.2}
\contentsline {chapter}{\numberline {3}Data description}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}The BET method}{18}{section.3.1}
\contentsline {section}{\numberline {3.2}The BET Questions}{19}{section.3.2}
\contentsline {section}{\numberline {3.3}Data used to test the system}{21}{section.3.3}
\contentsline {chapter}{\numberline {4}Proposed algorithm}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Pre-processing}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}Passage retrieval}{31}{section.4.2}
\contentsline {section}{\numberline {4.3}True-False Answer}{38}{section.4.3}
\contentsline {chapter}{\numberline {5}Evaluation methods}{42}{chapter.5}
\contentsline {section}{\numberline {5.1}Passage Retrieval Evaluation}{42}{section.5.1}
\contentsline {section}{\numberline {5.2}True-false Question Answering Evaluation}{43}{section.5.2}
\contentsline {section}{\numberline {5.3}Cross-Validation method}{44}{section.5.3}
\contentsline {chapter}{\numberline {6}Experimental Results}{46}{chapter.6}
\contentsline {section}{\numberline {6.1}Data Processing}{46}{section.6.1}
\contentsline {section}{\numberline {6.2}System Performance}{47}{section.6.2}
\contentsline {section}{\numberline {6.3}Experiments with ASR transcripts}{50}{section.6.3}
\contentsline {section}{\numberline {6.4}Summarization Results}{51}{section.6.4}
\contentsline {section}{\numberline {6.5}Parameter Optimization}{54}{section.6.5}
\contentsline {section}{\numberline {6.6}Comparison with BET scores obtained by human subjects}{57}{section.6.6}
\contentsline {chapter}{\numberline {7}Conclusion and future works}{63}{chapter.7}
\contentsline {chapter}{Appendix}{65}{chapter*.30}
\contentsline {section}{\numberline {7.1}Results for Cross-Validation}{65}{section.7.1}
\contentsline {section}{\numberline {7.2}List of stopwords}{67}{section.7.2}
\contentsline {section}{\numberline {7.3}Part-of-speech tags}{68}{section.7.3}
\contentsline {chapter}{Bibliography}{75}{chapter*.34}
