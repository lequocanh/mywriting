\contentsline {chapter}{Abstract}{1}{section*.1}
\contentsline {chapter}{Acknowledgements}{1}{chapter*.2}
\contentsline {chapter}{Table of contents}{3}{chapter*.3}
\contentsline {chapter}{List of Figures}{4}{chapter*.4}
\contentsline {chapter}{List of Tables}{5}{chapter*.5}
\contentsline {chapter}{List of Algorithms}{6}{chapter*.6}
\contentsline {chapter}{\numberline {1}Introduction}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Context}{7}{section.1.1}
\contentsline {section}{\numberline {1.2}Goal }{8}{section.1.2}
\contentsline {section}{\numberline {1.3}Approach }{8}{section.1.3}
\contentsline {section}{\numberline {1.4}Evaluation methods}{9}{section.1.4}
\contentsline {section}{\numberline {1.5}Structure of the report}{10}{section.1.5}
\contentsline {chapter}{\numberline {2}Related Work}{11}{chapter.2}
\contentsline {chapter}{\numberline {3}Data description}{16}{chapter.3}
\contentsline {section}{\numberline {3.1}The BET method}{16}{section.3.1}
\contentsline {section}{\numberline {3.2}The BET Questions}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}Data used to test the system}{19}{section.3.3}
\contentsline {chapter}{\numberline {4}Proposed algorithm}{23}{chapter.4}
\contentsline {section}{\numberline {4.1}Pre-processing}{23}{section.4.1}
\contentsline {section}{\numberline {4.2}Passage retrieval}{29}{section.4.2}
\contentsline {section}{\numberline {4.3}True-False Answer}{35}{section.4.3}
\contentsline {chapter}{\numberline {5}Evaluation methods}{39}{chapter.5}
\contentsline {section}{\numberline {5.1}Passage Retrieval Evaluation}{39}{section.5.1}
\contentsline {section}{\numberline {5.2}True-false Question Answering Evaluation}{40}{section.5.2}
\contentsline {section}{\numberline {5.3}Cross-Validation method}{41}{section.5.3}
\contentsline {chapter}{\numberline {6}Experimental Results}{42}{chapter.6}
\contentsline {section}{\numberline {6.1}Data Processing}{42}{section.6.1}
\contentsline {section}{\numberline {6.2}System Performance}{43}{section.6.2}
\contentsline {section}{\numberline {6.3}Experiments with ASR transcripts}{46}{section.6.3}
\contentsline {section}{\numberline {6.4}Summarization Results}{47}{section.6.4}
\contentsline {section}{\numberline {6.5}Parameter Optimization}{51}{section.6.5}
\contentsline {section}{\numberline {6.6}Comparison with BET scores by human subjects}{53}{section.6.6}
\contentsline {chapter}{\numberline {7}Future Research}{58}{chapter.7}
\contentsline {chapter}{\numberline {8}Conclusion}{59}{chapter.8}
\contentsline {chapter}{Appendix}{60}{chapter*.27}
\contentsline {section}{\numberline {8.1}Intermediate results for parameter optimisation}{60}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}IB4010}{60}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}IS1008c}{61}{subsection.8.1.2}
\contentsline {section}{\numberline {8.2}List of stopwords}{61}{section.8.2}
\contentsline {chapter}{Bibliography}{68}{chapter*.30}
