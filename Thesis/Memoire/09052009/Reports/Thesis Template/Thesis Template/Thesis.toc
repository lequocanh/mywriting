\contentsline {chapter}{Abstract}{i}{dummy.1}
\vspace {1em}
\contentsline {chapter}{Acknowledgements}{iii}{dummy.2}
\vspace {1em}
\contentsline {chapter}{List of Figures}{v}{dummy.4}
\contentsline {chapter}{List of Tables}{vi}{dummy.6}
\contentsline {chapter}{Abbreviations}{viii}{dummy.9}
\vspace {2em}
\contentsline {chapter}{\numberline {1}Chapter Title Here}{1}{chapter.12}
\contentsline {section}{\numberline {1.1}Welcome and Thank You}{1}{section.13}
\contentsline {subsection}{\numberline {1.1.1}Examples of \LaTeX {} Typeset eBooks}{1}{subsection.14}
\contentsline {section}{\numberline {1.2}Learning \LaTeX {}}{2}{section.15}
\contentsline {subsection}{\numberline {1.2.1}A (not so short) Introduction to \LaTeX {}}{2}{subsection.16}
\contentsline {subsection}{\numberline {1.2.2}A Short Math Guide for \LaTeX {}}{2}{subsection.17}
\contentsline {subsection}{\numberline {1.2.3}Common \LaTeX {} Math Symbols}{2}{subsection.18}
\contentsline {subsection}{\numberline {1.2.4}\LaTeX {} on a Mac}{3}{subsection.19}
\contentsline {section}{\numberline {1.3}Getting Started with this Template}{3}{section.20}
\contentsline {subsection}{\numberline {1.3.1}Latest Version}{3}{subsection.21}
\contentsline {subsection}{\numberline {1.3.2}List of Changes}{4}{subsection.22}
\contentsline {subsection}{\numberline {1.3.3}About this Template}{4}{subsection.28}
\contentsline {section}{\numberline {1.4}What this Template Includes}{5}{section.29}
\contentsline {subsection}{\numberline {1.4.1}Folders}{5}{subsection.30}
\contentsline {subsection}{\numberline {1.4.2}Files}{6}{subsection.31}
\contentsline {section}{\numberline {1.5}Filling in the `\texttt {Thesis.cls}' File}{8}{section.32}
\contentsline {section}{\numberline {1.6}The `\texttt {Thesis.tex}' File Explained}{8}{section.33}
\contentsline {section}{\numberline {1.7}Thesis Features and Conventions}{10}{section.34}
\contentsline {subsection}{\numberline {1.7.1}Printing Format}{10}{subsection.35}
\contentsline {subsection}{\numberline {1.7.2}Using US Letter Paper}{10}{subsection.36}
\contentsline {subsection}{\numberline {1.7.3}References}{11}{subsection.37}
\contentsline {subsection}{\numberline {1.7.4}Figures}{11}{subsection.39}
\contentsline {subsection}{\numberline {1.7.5}Typesetting mathematics}{13}{subsection.42}
\contentsline {section}{\numberline {1.8}Sectioning and Subsectioning}{14}{section.44}
\contentsline {section}{\numberline {1.9}In Closing}{14}{section.45}
\vspace {2em}
\contentsline {chapter}{\numberline {A}Appendix Title Here}{15}{appendix.47}
\vspace {2em}
\contentsline {chapter}{Bibliography}{16}{dummy.48}
