\contentsline {chapter}{Abstract}{1}{section*.1}
\contentsline {chapter}{Acknowledgements}{3}{chapter*.3}
\contentsline {chapter}{Table of contents}{5}{chapter*.4}
\contentsline {chapter}{List of Figures}{6}{chapter*.5}
\contentsline {chapter}{List of Tables}{8}{chapter*.6}
\contentsline {chapter}{List of Algorithms}{9}{chapter*.7}
\contentsline {chapter}{\numberline {1}Introduction}{10}{chapter.1}
\contentsline {section}{\numberline {1.1}Context}{10}{section.1.1}
\contentsline {section}{\numberline {1.2}Goal }{11}{section.1.2}
\contentsline {section}{\numberline {1.3}Approach}{11}{section.1.3}
\contentsline {section}{\numberline {1.4}Evaluation methods}{12}{section.1.4}
\contentsline {section}{\numberline {1.5}Structure of the report}{13}{section.1.5}
\contentsline {chapter}{\numberline {2}Related Work}{14}{chapter.2}
\contentsline {chapter}{\numberline {3}Data description}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}The BET method}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}The BET Questions}{20}{section.3.2}
\contentsline {section}{\numberline {3.3}Data used to test the system}{22}{section.3.3}
\contentsline {chapter}{\numberline {4}Proposed algorithm}{26}{chapter.4}
\contentsline {section}{\numberline {4.1}Pre-processing}{26}{section.4.1}
\contentsline {section}{\numberline {4.2}Passage retrieval}{32}{section.4.2}
\contentsline {section}{\numberline {4.3}True-False Answer}{39}{section.4.3}
\contentsline {chapter}{\numberline {5}Evaluation methods}{43}{chapter.5}
\contentsline {section}{\numberline {5.1}Passage Retrieval Evaluation}{43}{section.5.1}
\contentsline {section}{\numberline {5.2}True-false Question Answering Evaluation}{44}{section.5.2}
\contentsline {section}{\numberline {5.3}Cross-Validation method}{45}{section.5.3}
\contentsline {chapter}{\numberline {6}Experimental Results}{47}{chapter.6}
\contentsline {section}{\numberline {6.1}Data Processing}{47}{section.6.1}
\contentsline {section}{\numberline {6.2}System Performance}{48}{section.6.2}
\contentsline {section}{\numberline {6.3}Experiments with ASR transcripts}{51}{section.6.3}
\contentsline {section}{\numberline {6.4}Summarization Results}{52}{section.6.4}
\contentsline {section}{\numberline {6.5}Parameter Optimization}{56}{section.6.5}
\contentsline {section}{\numberline {6.6}Comparison with BET scores obtained by human subjects}{58}{section.6.6}
\contentsline {chapter}{\numberline {7}Conclusion and future works}{64}{chapter.7}
\contentsline {chapter}{Appendix}{66}{chapter*.30}
\contentsline {section}{\numberline {7.1}Results for Cross-Validation}{66}{section.7.1}
\contentsline {section}{\numberline {7.2}List of stopwords}{68}{section.7.2}
\contentsline {section}{\numberline {7.3}Part-of-speech tags}{69}{section.7.3}
\contentsline {chapter}{Bibliography}{76}{chapter*.34}
